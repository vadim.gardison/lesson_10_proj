/*

  localStorage
  window.localStorage

*/

// Запись в ЛС
// localStorage.setItem('myCat', 'Tom');
// localStorage.setItem('back', 'green');
// // Чтение с ЛС
// var cat = localStorage.getItem("myCat");
// // Удаление с ЛС
// // localStorage.removeItem("myCat");

// // Если не найдено, вернет Null
// var background = localStorage.getItem("back");
// console.log(background);

// if( background !== null){
//   document.body.style.backgroundColor = background;
// }

// socket.io

/*

  Задание 1:
    Написать скрипт, который по клику на кнопку рандомит цвет и записывает его в localStorage
    После перезагрузки страницы, цвет должен сохранится.

  Задание 2:
    Написать форму логина (логин пароль), которая после отправки данных записывает их в localStorage.
    Если в localStorage есть записть - На страниче вывести "Привет {username}", если нет - вывести окно
    логина.

    + бонус, сделать кнопку "выйти" которая удаляет запись из localStorage и снова показывает форму логина
    + бонус сделать проверку логина и пароля на конкретную запись. Т.е. залогинит пользователя если
    он введет только admin@example.com и пароль 12345678
*/

// first task
function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getRandomColorFirst(r, g, b) {
  console.log(r, g, b);
  return 'rgb(' + r + ',' + g + ',' + b + ')';
}

function toHex(c){
  if (c > 15) { return c.toString(16) }
  else { return "0" + c.toString(16) }
}

function getRandomColorSecond(r, g, b) {
  return '#' + toHex(r) + toHex(g) + toHex(b)
}

function applyElColorFirst() {
  var elementColor = getRandomColorFirst(getRandomIntInclusive(0, 257), getRandomIntInclusive(0, 257), getRandomIntInclusive(0, 257));
  firstContainer.style.backgroundColor = elementColor;
  p.innerText = elementColor;
  localStorage.setItem('bgColorFirst', `${elementColor}`);
}

function applyElColorSecond() {
  var elementColorSecond = getRandomColorSecond(getRandomIntInclusive(0, 257), getRandomIntInclusive(0, 257), getRandomIntInclusive(0, 257));
  secondContainer.style.backgroundColor = elementColorSecond;
  pS.innerText = elementColorSecond;
  localStorage.setItem('bgColorSecond', `${elementColorSecond}`);
}

var mainDiv = document.getElementById('App');
mainDiv.style.height = '-webkit-fill-available';

// containers
var firstContainer = document.createElement('div');
firstContainer.id = 'firstId';
mainDiv.appendChild(firstContainer);
var secondContainer = document.createElement('div');
mainDiv.appendChild(secondContainer);

// Buttons
var firstButt = document.createElement('button');
firstContainer.appendChild(firstButt);
firstContainer.style.height = '20em';
firstButt.style.marginLeft = '45%';
firstButt.style.marginRight = '45%';
firstButt.innerText = 'Press me to change bg color';
firstButt.setAttribute('onClick', 'applyElColorFirst()');
firstButt.setAttribute('id', 'butt');

var secondButt = document.createElement('button');
secondContainer.appendChild(secondButt);
secondContainer.style.height = '20em';
secondButt.style.marginLeft = '45%';
secondButt.style.marginRight = '45%';
secondButt.innerText = 'Press me to change bg color';
secondButt.setAttribute('onClick', 'applyElColorSecond()');
secondButt.setAttribute('id', 'buttSec');

// p
var p = document.createElement('p');
p.style.margin = 'auto';
p.style.width = '10%';
p.style.backgroundColor = 'antiquewhite';
firstContainer.appendChild(p);
var pS = document.createElement('p');
pS.style.margin = 'auto';
pS.style.width = '10%';
pS.style.backgroundColor = 'antiquewhite';
secondContainer.appendChild(pS);

// onload event
window.onload = function() {
  firstContainer.style.backgroundColor = localStorage.getItem('bgColorFirst');
  secondContainer.style.backgroundColor = localStorage.getItem('bgColorSecond');
  // applyElColorFirst();
  // applyElColorSecond();
}

// second task
